
;;; X-macro formatter written in Python
;;; code based on https://singletonresearch.com/2019/10/02/emacs-external-code-formatting-in-python/
 (defgroup xmacro nil
   "Tools for X-macro programming."
   :prefix "xmacro-"
   :group 'applications)
 (defcustom xmacro-formatter-program "/home/izzard/svn/python/formatting/xmacros.py"
   "External program that formats X-macro code."
   :type '(string)
   :group 'xmacro)
 (defun xmacro-format ()
   "Format the current buffer using X-macro formatting."
   (interactive)
   (save-excursion
     (shell-command-on-region (point-min) (point-max) xmacro-formatter-program t t)
     (whitespace-cleanup)
     ))
 (provide 'xmacro-formatter)
 ;;; 
