#!/usr/bin/env python3

"""
Script to clean up single-line X-macro tables in C, formatting all into correct-length columns.

Written by Robert Izzard 2023 who accepts no liability and gives no guarantee this
will work.

Example: convert this messy X-macro list

---
#define NUCSYN_ISOTOPE_LIST_LIGHT             \
    X(n,    0,1,1.67493e-24)                    \
    X(e,0,0,   9.10938e-28)                     \
    X(H1,1,1,1.67353e-24)                       \
    X( H2,1,2,3.3445e-24)
---

to this which is neat and human-readable

---
#define NUCSYN_ISOTOPE_LIST_LIGHT \
    X(    n,  0,  1, 1.67493e-24) \
    X(    e,  0,  0, 9.10938e-28) \
    X(   H1,  1,  1, 1.67353e-24) \
    X(   H2,  1,  2,  3.3445e-24)
---

Command-line use:

---

cat sourcefile.c | xmacros.py

---


Or import into emacs with the following in ~/.emacs-el/xmacro-formatter.el:

---

;;; X-macro formatter written in Python
 (defgroup xmacro nil
   "Tools for X-macro programming."
   :prefix "xmacro-"
   :group 'applications)
 (defcustom xmacro-formatter-program "/path/to/xmacros.py"
   "External program that formats X-macro code."
   :type '(string)
   :group 'xmacro)
 (defun xmacro-format ()
   "Format the current buffer using X-macro formatting."
   (interactive)
   (save-excursion
     (shell-command-on-region (point-min) (point-max) xmacro-formatter-program t t)
     (whitespace-cleanup)
     ))
 (provide 'xmacro-formatter)
 ;;;

---

and the following in .emacs to load the module and add it to a menu bar

---
; load custom X-macro formatter
(require 'xmacro-formatter)

; add to emacs' menu bar
(require 'xmacro-formatter)
(define-key-after global-map
  [menu-bar extra-tools]
  (cons "Extra Tools"
        (easy-menu-create-menu "Extra Tools" nil))
  'tools)
(easy-menu-define my-delim-col-menu nil "Menu for Delim Col"
  '("Delimit Columns in ..."
     ["X-macros" xmacro-format :help "Prettify all columns in a text region"]))
(easy-menu-add-item (current-global-map) '("menu-bar" "extra-tools") my-delim-col-menu)
---

# code inspired by
# https://stackoverflow.com/questions/241327/remove-c-and-c-comments-using-python
# https://singletonresearch.com/2019/10/02/emacs-external-code-formatting-in-python/

"""


import re
import sys
from pygments import lex
from pygments.token import Token as ParseToken
from pygments.lexers.c_cpp import CLexer

Xleading_space=4
spacing=1
opening_brackets = ('(','[','{')
closing_brackets = (')',']','}')
vb = False

def chunks(replace_query, lexer=CLexer()):
    # split a line of C, containing the X macro, into
    # code chunks
    generator = lex(replace_query, lexer)
    tokens = []
    punctuation = []
    depth = 0 # expression depth
    n = 0 # number of commas at depth==1
    replace_query = replace_query.rstrip()

    for token in generator:
        #print(f"Token: {token}")
        token_type = token[0]
        token_text = token[1]

        # ignore final newline
        if token_text == '\n':
            continue

        # strip everything before \\ at the end of a line
        if token_text == '\\\n':
            token_text = token_text.lstrip()

        # opening of an expression
        if token_type in ParseToken.Punctuation and \
           (token_text in opening_brackets):
            depth += 1

        # check if we're in the outermost code
        if depth == 1:

            # strip whitespace
            if n == 0 or token_text.endswith('\\'):
                token_text = token_text.rstrip()

            # this is a separator, reset
            if token_text == ',':
                n = 0

            # ignore most punctuation
            if token_type in ParseToken.Punctuation and \
               token_text != '.':
                punctuation.append(token_text)
                tokens.append('')
                continue

        # closing of an expression
        if token_type in ParseToken.Punctuation and \
           (token_text in closing_brackets):
            depth -= 1

        # strip whitespace if in outermost depth
        if depth == 1:
            token_text = token_text.strip()

        # put everything else into an element of tokens list
        n += 1
        if len(tokens) == 0:
            tokens = [token_text]
        else:
            tokens[-1] += token_text

    # remove whitespace
    if tokens:
        #  trailing newline
        tokens[-1] = tokens[-1].rstrip()
    return (tokens,punctuation)

def do_main():
    """Main function"""

    buffer = []
    xmacros_buffer = []

    # get all stdin, force a trailing line
    # so we flush buffers
    stdin = []
    for line in map(str.rstrip,sys.stdin):
        stdin.append(line)
    stdin.append("")

    # loop and process
    for line in stdin:
        lstripped_line = line.lstrip()

        # if an X macro, add to xmacros_buffer
        if lstripped_line.startswith('X(') or \
           (len(xmacros_buffer) > 0 and lstripped_line.endswith('\\')):
            if len(xmacros_buffer) == 0 and buffer:
                prev_buffer = []
                # we need to include the previous line(s)
                # in the macro definition
                if buffer and len(buffer)>0:
                    while True:
                        if not buffer or not buffer[-1].endswith('\\'):
                            break
                        prev_buffer.append(buffer.pop())

                prev_buffer.reverse()
                xmacros_buffer += prev_buffer
            xmacros_buffer.append(lstripped_line)

        else:
            # process xmacro buffer

            # first we need all column widths
            lenmax = {}
            for xmacro in xmacros_buffer:
                # split into code chunks
                (xmacro_chunks,xmacro_punctuation) = chunks(xmacro)

                if len(xmacro_chunks) == 0:
                    continue

                if vb:
                    print(f"LINE  : {xmacro}")
                    print(f"CHUNKS: {xmacro_chunks}")
                    print(f"PUNCTS: {xmacro_punctuation}")

                # detect X(...) lines
                if xmacro_chunks[0] == 'X':
                    # save the widths of chunks
                    for i,chunk in enumerate(xmacro_chunks):
                        l = len(chunk)
                        #print(f"Chunk {l} : \"{chunk}\"")
                        if i != 0:
                            l += spacing
                        if i in lenmax:
                            lenmax[i] = max(lenmax[i], l)
                        else:
                            lenmax[i] = l

            # compute total line width
            line_width = 2
            for l in lenmax.values():
                line_width += spacing + l

            # check all lines fit
            for xmacro in xmacros_buffer:
                (xmacro_chunks,xmacro_punctuation) = chunks(xmacro)
                if len(xmacro_chunks) == 0:
                    continue
                if not xmacro_chunks[0] == 'X':
                    m = re.match('(#define\s+\S+)\s+\\\\',xmacro)
                    if m:
                        # probably the macro list definition
                        line_width = max(line_width,len(m.group(1)) + spacing)
                    else:
                        line_width = max(line_width,len(xmacro) + spacing)

            # parse again to output the table of macros
            for xmacro in xmacros_buffer:
                (xmacro_chunks,xmacro_punctuation) = chunks(xmacro)
                if len(xmacro_chunks) == 0:
                    continue

                new_xmacro_chunks = xmacro_chunks
                if xmacro_chunks[0] == 'X':

                    # reformat the X macro chunks
                    for i,chunk in enumerate(xmacro_chunks):
                        l = lenmax[i]
                        if i == 0:
                            l += Xleading_space
                        new_xmacro_chunks[i] = f"{xmacro_chunks[i]:>{l}}"

                    # reconstruct the X macro
                    new_string = ''
                    last = len(new_xmacro_chunks) - 1 # ignore last \
                    for i,x in enumerate(new_xmacro_chunks):
                        if i != last:
                            new_string += new_xmacro_chunks[i] + xmacro_punctuation[i]
                    new_string = f"{new_string:{line_width}}" + new_xmacro_chunks[-1].strip()
                    buffer.append(new_string)

                else:
                    # not an X macro, perhaps a #define or comment?
                    if line_width > 0:
                        m = re.match('(#define\s+\S+)\s+\\\\',xmacro)
                        if m:
                            # probably the macro list definition
                            buffer.append(f"{m.group(1):<{line_width}}\\")
                        else:
                            # other e.g. comment
                            buffer.append(f"{xmacro:>{line_width+1}}")
                    else:
                        buffer.append(xmacro)

            xmacros_buffer = []
            buffer.append(line)

    for line in buffer:
        print(line)

if __name__ == '__main__':
    do_main()
