# X-macros in Emacs


A script and emacs integration to clean up single-line X-macro tables in C, formatting all into correct-length columns.

For example, it converts something ugly like this

```
#define NUCSYN_ISOTOPE_LIST_LIGHT               \
    X(n,0,1,1.67493e-24)                        \
    X(e,0,0,9.10938e-28)                        \
    X(H1,1,1,1.67353e-24)                       \
    X(H2,1,2,3.3445e-24)
```

to something pretty like this

```
#define NUCSYN_ISOTOPE_LIST_LIGHT \
    X(    n,  0,  1, 1.67493e-24) \
    X(    e,  0,  0, 9.10938e-28) \
    X(   H1,  1,  1, 1.67353e-24) \
    X(   H2,  1,  2,  3.3445e-24)
```

## Use

Command-line use:

```
cat sourcefile.c | xmacros.py
```


Or import into emacs with the following in `~/.emacs-el/xmacro-formatter.el`:

```
;;; X-macro formatter written in Python
 (defgroup xmacro nil
   "Tools for X-macro programming."
   :prefix "xmacro-"
   :group 'applications)
 (defcustom xmacro-formatter-program "/path/to/xmacros.py"
   "External program that formats X-macro code."
   :type '(string)
   :group 'xmacro)
 (defun xmacro-format ()
   "Format the current buffer using X-macro formatting."
   (interactive)
   (save-excursion
     (shell-command-on-region (point-min) (point-max) xmacro-formatter-program t t)
     (whitespace-cleanup)
     ))
 (provide 'xmacro-formatter)
 ;;;
```

and the following in `.emacs` to load the module and add it to a menu bar

```
; load custom X-macro formatter
(require 'xmacro-formatter)

; add to emacs' menu bar
(require 'xmacro-formatter)
(define-key-after global-map
  [menu-bar extra-tools]
  (cons "Extra Tools"
        (easy-menu-create-menu "Extra Tools" nil))
  'tools)
(easy-menu-define my-delim-col-menu nil "Menu for Delim Col"
  '("Delimit Columns in ..."
     ["X-macros" xmacro-format :help "Prettify all columns in a text region"]))
(easy-menu-add-item (current-global-map) '("menu-bar" "extra-tools") my-delim-col-menu)
```

Written by Robert Izzard, 2023. Comes with no warranty whatsoever: use at your own risk and likely to be buggy. Certainly will not work on multi-line X macros.

Code inspired by
https://stackoverflow.com/questions/241327/remove-c-and-c-comments-using-python
https://singletonresearch.com/2019/10/02/emacs-external-code-formatting-in-python/


