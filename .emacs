;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; This should be copied into your .emacs file
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; custom X-macro formatter
(require 'xmacro-formatter)

; custom menu bar
(define-key-after global-map
  [menu-bar extra-tools]
  (cons "Extra Tools"
        (easy-menu-create-menu "Extra Tools" nil))
  'tools)
(easy-menu-define my-delim-col-menu nil "Menu for Delim Col"
  '("Delimit Columns in ..."
     ["X-macros" xmacro-format :help "Prettify all columns in a text region"]))
(easy-menu-add-item (current-global-map) '("menu-bar" "extra-tools") my-delim-col-menu)
